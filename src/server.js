const express = require('express');
const app = express();
const routes = require('./routes');
const dynamo = require('./infraestructure/connection');

const loadData = require('./mock/loadTable');
const coworking = require('./models/Coworking');
const dataMock = require('./mock/Data');

require('dotenv').config();
app.use(express.json());
app.use(routes);
app.listen(3000);

dynamo.listTables((err, data) => {
  if(data.TableNames.some(x => 'Coworking')) return;
  try {
    loadData.loadTable(dynamo, coworking, dataMock);
  } catch(e){
    console.log(e)
  }
});