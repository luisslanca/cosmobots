require('dotenv').config();
const dynamo = require('../infraestructure/connection');

module.exports.getAllCoworking = function (locals) {
  return new Promise((resolve, reject) => {
    dynamo.batchGetItem(locals, (err, data) => {
      if (err) {
        console.error(err);
      } else {
        resolve(data.Responses.Coworking);
      }
    });
  })
};

module.exports.AvaliableCoworking = function(lat, lng) {
  return findCoordinates(lat, lng);
};

function findCoordinates(lat, lng) {
  return new Promise((resolve, reject) => {
    dynamo.scan({ TableName: 'Coworking' }, (err, data) => {
      if(err) throw err;
      let proximity = [];
      data.Items.forEach(item => {
        let pointA = Math.pow((Number(item.lat.S) - lat), 2);
        let pointB = Math.pow((Number(item.lng.S) - lng), 2);
        let distance = Math.sqrt(pointA + pointB);
        proximity.push({ item, distance });
      });

      let sorted = proximity.sort((a, b) => {
        return a.distance - b.distance;
      })
      resolve(({ item } = sorted)[0]);
    });
  })
};

console.log(dynamo);