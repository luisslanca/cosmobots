const AWS = require('aws-sdk');
require('dotenv').config();

const dynamo = new AWS.DynamoDB({
  apiVersion: '2019-10-31',
  accessKeyId: process.env.aws_access_key_id,
  secretAccessKey: process.env.aws_secret_access_key,
  region: 'us-east-2',
});

module.exports = dynamo;