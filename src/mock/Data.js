var items = {
  RequestItems: {
    "Coworking": [
      {
        PutRequest: {
          Item: {
            "name": {
              "S": "Coworking São Paulo"
            },
            "address": {
              "S": "Does Not Matter, 123"
            },
            "Country": {
              "S": "BR"
            },
            "city": {
              "S": "São Paulo"
            },
            "state": {
              "S": "SP"
            },
            "zipcode": {
              "S": "01000000"
            },
            "lat": {
              "S": "-23.533773"
            },
            "lng": {
              "S": "-46.625290"
            }
          }
        }
      },
      {
        PutRequest: {
          Item: {
            "name": {
              "S": "Coworking Rio de Janeiro"
            },
            "address": {
              "S": "Does Not Matter, 123"
            },
            "Country": {
              "S": "BR"
            },
            "city": {
              "S": "Rio de Janeiro"
            },
            "state": {
              "S": "RJ"
            },
            "zipcode": {
              "S": "05400100"
            },
            "lat": {
              "S": "-22.908333"
            },
            "lng": {
              "S": "-43.196388"
            }
          }
        }
      },
      {
        PutRequest: {
          Item: {
            "name": {
              "S": "Coworking New York"
            },
            "address": {
              "S": "Good Luck, 567"
            },
            "Country": {
              "S": "USA"
            },
            "city": {
              "S": "New York City"
            },
            "state": {
              "S": "NY"
            },
            "zipcode": {
              "S": "10001000"
            },
            "lat": {
              "S": "40.730610"
            },
            "lng": {
              "S": "-73.935242"
            }
          }
        }
      },
      {
        PutRequest: {
          Item: {
            "name": {
              "S": "Coworking San Francisco"
            },
            "address": {
              "S": "Enjoy, 789"
            },
            "Country": {
              "S": "USA"
            },
            "city": {
              "S": "San Francisco"
            },
            "state": {
              "S": "CA"
            },
            "zipcode": {
              "S": "94110000"
            },
            "lat": {
              "S": "37.733795"
            },
            "lng": {
              "S": "-122.446747"
            }
          }
        }
      }
    ]
  }
};

module.exports = items;