module.exports.loadTable = function (dynamo, model, dataMock) {
  let passo = 0;

  dynamo.createTable(model, (err, data) => {
    if (err) throw err;
    console.log('Criei com sucesso blastoise!!');
  }).on('complete', () => {

    let interval = setInterval(() => {
      dynamo.describeTable({ TableName: 'Coworking' }, (err, data) => {
        console.log('Analisando... ' + (++passo));
        if (err || data === null) return;

        if (data.Table.TableStatus === "ACTIVE") {
          dynamo.batchWriteItem(dataMock, (err, data) => {
            if (err) throw err;
            console.log('escrevi os itens');
            clearInterval(interval);
          });
        }
      });
    }, 2000);
  });
};