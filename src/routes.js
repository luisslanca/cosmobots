const express = require('express');
const routes = express.Router();

const CoworkingController = require('./controllers/CoworkingController');

routes.post('/AvaliableCoworking', (req, res) => {
  let coordinates = req.body;

  if (coordinates === null || coordinates === {}) {
    return;
  }
  if (coordinates.myLocation === null || coordinates.myLocation === {}) {
    return;
  }
  
  CoworkingController.AvaliableCoworking(Number(coordinates.myLocation.lat), Number(coordinates.myLocation.lng))
    .then(item => {
    res.send(item);
  })
})

module.exports = routes;