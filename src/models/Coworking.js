var coworking = {
  TableName: "Coworking",
  AttributeDefinitions: [
    { AttributeName: "name", AttributeType: "S" }
  ],
  KeySchema: [
    { AttributeName: "name", KeyType: "HASH" }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 5,
    WriteCapacityUnits: 5
  }
};

module.exports = coworking;

// Objeto que representa a tabela do Banco (Um espelho) - MODEL